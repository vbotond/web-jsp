<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Time Page</title>
</head>
<body>
	<h1>Time Page</h1>
	
	<form action="TimeServlet" method="get">
		<input name="number" type="text">
		<input type="submit">
	</form>
	
	<br>
	
	Number: ${param.number}<br>
	<c:choose>
		<c:when test="${param.number lt 10}">
			This is less than 10
		</c:when>
		<c:when test="${param.number gt 10}">
			This is greater than 10
		</c:when>
		<c:otherwise>
			This must be 10 then!
		</c:otherwise>
	</c:choose>
</body>
</html>